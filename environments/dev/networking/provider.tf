provider "aws" {
  shared_credentials_file = "../aws_credentials"
  profile                 = "default"
  region                  = "eu-west-1"
}

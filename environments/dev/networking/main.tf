module "vpc" {
  source = "../../../modules/vpc"

  vpc_instance_tenancy = "${var.vpc_instance_tenancy}"
  vpc_cidr_block = "${var.vpc_cidr_block}"
  vpc_enable_dns_support = "${var.vpc_enable_dns_support}"
  vpc_enable_dns_hostnames = "${var.vpc_enable_dns_hostnames}"
  vpc_enable_classiclink = "${var.vpc_enable_classiclink}"
  environment_name = "${var.environment_name}"
}

module "elastic_ip" {
  source = "../../../modules/elastic_ip"
  environment_name = "${var.environment_name}"
}
module "public_subnet" {
  source = "../../../modules/subnets"

  vpc_id = "${module.vpc.id}"
  subnet_cidr_block = "${var.subnet_public_cidr_block}"
  subnet_name = "public-${var.environment_name}"
}
module "private_subnet" {
  source = "../../../modules/subnets"
  
  vpc_id = "${module.vpc.id}"
  subnet_cidr_block = "${var.subnet_private_cidr_block}"
  subnet_name = "private-${var.environment_name}"
}
module "internet_network" {
  source = "../../../modules/internet_network"
  
  vpc_id = "${module.vpc.id}"
  elastic_ip_id = "${module.elastic_ip.id}"
  public_subnets = "${module.public_subnet.id}"
  vpc_default_route_table_id = "${module.vpc.default_route_table_id}"
  environment_name = "${var.environment_name}"
}
module "public_route_table_association" {
  source = "../../../modules/route_table_association"

  subnet_id = "${module.public_subnet.id}"
  route_table_id = "${module.internet_network.public_route_table_id}"
}
module "private_route_table_association" {
  source = "../../../modules/route_table_association"

  subnet_id = "${module.private_subnet.id}"
  route_table_id = "${module.vpc.default_route_table_id}"
}

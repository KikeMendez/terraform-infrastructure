variable "environment_name" {}
variable "vpc_instance_tenancy" {}
variable "vpc_cidr_block" {}
variable "vpc_enable_dns_support" {}
variable "vpc_enable_dns_hostnames" {}
variable "subnet_public_cidr_block" { type = "list" }
variable "subnet_private_cidr_block" { type = "list" }
variable "vpc_enable_classiclink" {}

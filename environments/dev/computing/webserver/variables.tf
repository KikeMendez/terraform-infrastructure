variable "environment_name" {}
variable "webserver_key_name" {}
variable "webserver_public_key" {}
variable "webserver_instance_ami" {}
variable "webserver_instance_type" {}

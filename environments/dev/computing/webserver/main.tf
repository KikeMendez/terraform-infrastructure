module "webserver_sg" {
  source = "../../../../modules/security_group"

  bastion_security_group = false
  vpc_id = "${data.aws_vpcs.vpc.ids[0]}"
  sg_name = "webserver-${var.environment_name}"
  description = "webserver security group only allow ssh connection from bastion"
}
module "webserver_sg_rule_port_80_inbound" {
  source = "../../../../modules/security_group_rule"
  sgr_cidr_block = true
  
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_block = ["0.0.0.0/0"]
  security_group_id = "${module.webserver_sg.id[0]}"
}
module "webserver_sg_rule_port_80_outbound" {
  source = "../../../../modules/security_group_rule"
  sgr_cidr_block = true
  
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_block = ["0.0.0.0/0"]
  security_group_id = "${module.webserver_sg.id[0]}"
}
module "webserver_sg_rule_port_22_inbound" {
  source = "../../../../modules/security_group_rule"
  sgr_cidr_block = false
  
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = "${module.webserver_sg.id[0]}"
  source_security_group_id = "${data.aws_security_groups.bastion.ids[0]}"
}
module "web_server_key_pair" {
  source = "../../../../modules/key_pair"
  key_name = "${var.webserver_key_name}"
  public_key = "${var.webserver_public_key}"
}
module "web_server" {
    source = "../../../../modules/ec2"
    associate_public_ip_address = true
    instance_ami = "${var.webserver_instance_ami}"
    instance_type = "${var.webserver_instance_type}"
    instance_key_name = "${var.webserver_key_name}"
    vpc_security_group_ids = ["${module.webserver_sg.id[0]}"]
    instance_subnet_id = "${data.aws_subnet_ids.public.ids[0]}"
    user_data = "${data.template_file.init_webserver.rendered}"
    instance_tag_name = "web"
    instance_name = "${var.environment_name}"
}

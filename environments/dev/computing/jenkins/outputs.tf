output "vpc id:" {
  value = "${data.aws_vpcs.vpc.ids}"
}

output "bastion sg:" {
  value = "${data.aws_security_groups.bastion.ids}"
}

output "public subnet:" {
  value = "${data.aws_subnet_ids.public.ids}"
}

module "jenkins_master_security_group" {
  source = "../../../../modules/security_group"

  bastion_security_group = false
  vpc_id = "${data.aws_vpcs.vpc.ids[0]}"
  sg_name = "jenkins_master-${var.environment_name}"
  description = "This security group allows ssh from bastion and port 8080 from anywhere"
}
module "jenkins_master_security_group_rule_8080_inbound" {
  source = "../../../../modules/security_group_rule"
  type = "ingress"
  protocol = "tcp"
  sgr_cidr_block = true
  from_port = 8080
  to_port = 8080
  cidr_block = ["0.0.0.0/0"]
  security_group_id = "${module.jenkins_master_security_group.id[0]}"
}
module "jenkins_master_security_group_rule_50000_inbound" {
  source = "../../../../modules/security_group_rule"
  type = "ingress"
  protocol = "tcp"
  sgr_cidr_block = true
  from_port = 50000
  to_port = 50000
  cidr_block = ["0.0.0.0/0"]
  security_group_id = "${module.jenkins_master_security_group.id[0]}"
}
module "jenkins_master_security_group_rule_22_inbound" {
  source = "../../../../modules/security_group_rule"
  type = "ingress"
  protocol = "tcp"
  sgr_cidr_block = false
  from_port = 22
  to_port = 22
  security_group_id = "${module.jenkins_master_security_group.id[0]}"
  source_security_group_id = "${data.aws_security_groups.bastion.ids[0]}"
}
module "jenkins_master_security_group_rule_all_outbound" {
  source = "../../../../modules/security_group_rule"
  type = "egress"
  protocol = "-1"
  sgr_cidr_block = true
  from_port = 0
  to_port = 0
  cidr_block = ["0.0.0.0/0"]
  security_group_id = "${module.jenkins_master_security_group.id[0]}"
}
module "jenkins_master_key_pair" {
  source = "../../../../modules/key_pair"
  key_name = "${var.jenkins_master_key_name}"
  public_key = "${var.jenkins_master_public_key}"
}
module "jenkins_master_server" {
  source = "../../../../modules/ec2"
  associate_public_ip_address = true
  instance_ami = "${var.jenkins_master_instance_ami}"
  instance_type = "${var.jenkins_master_instance_type}"
  instance_key_name = "${var.jenkins_master_key_name}"
  user_data = "${data.template_file.init_jenkins_master.rendered}"
  instance_subnet_id = "${data.aws_subnet_ids.public.ids[0]}"
  instance_tag_name = "jenkins_master"
  instance_name = "${var.environment_name}"
  vpc_security_group_ids = ["${module.jenkins_master_security_group.id[0]}"]
}

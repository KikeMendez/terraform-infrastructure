variable "environment_name" {}
variable "jenkins_master_key_name" {}
variable "jenkins_master_public_key" {}

variable "jenkins_master_instance_ami" {}
variable "jenkins_master_instance_type" {}

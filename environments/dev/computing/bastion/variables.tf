
variable "environment_name" {}
variable "bastion_inbound_ip" { type = "list" }
variable "bastion_key_name" {}
variable "bastion_public_key" {}
variable "bastion_instance_ami" {}
variable "bastion_instance_type" {}

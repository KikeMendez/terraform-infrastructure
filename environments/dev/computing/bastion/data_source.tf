data "aws_vpcs" "vpc" {
  tags = {
    Name= "vpc_${var.environment_name}"
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpcs.vpc.ids[0]}"

  tags = {
    Name = "public-${var.environment_name}"
  }
}

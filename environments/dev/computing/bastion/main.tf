module "bastion_sg" {
  source = "../../../../modules/security_group"
    bastion_security_group = true 
    vpc_id = "${data.aws_vpcs.vpc.ids[0]}"
    sg_name = "bastion-${var.environment_name}"
    description = "This security group allows ssh connection only for the IP address specified in the environment variables file"
    ingress_cidr_blocks = "${var.bastion_inbound_ip}"
}

module "bastion_key_pair" {
  source = "../../../../modules/key_pair"
  key_name = "${var.bastion_key_name}"
  public_key = "${var.bastion_public_key}"
}

module "bastion_server" {
  source = "../../../../modules/ec2"
  is_bastion_server = true
  associate_public_ip_address = true
  instance_ami = "${var.bastion_instance_ami}"
  instance_type = "${var.bastion_instance_type}"
  instance_key_name = "${var.bastion_key_name}"
  vpc_security_group_ids = ["${module.bastion_sg.bastion_sg_id[0]}"]
  instance_subnet_id = "${data.aws_subnet_ids.public.ids[0]}"
  instance_tag_name = "bastion"
  instance_name = "${var.environment_name}"
}

#!/bin/bash
sudo su
apt-get update
apt-get install make
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
usermod -a -G docker ubuntu && su ubuntu
mkdir /home/ubuntu/jenkins

cd /home/ubuntu/ && git clone https://gitlab.com/KikeMendez/docker-jenkins-experiment.git
sudo chown ubuntu:ubuntu docker-jenkins-experiment/
sudo chown ubuntu:ubuntu /home/ubuntu/jenkins/
cd docker-jenkins-experiment && make build && make start

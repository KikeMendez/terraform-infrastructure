# GLOBAL CONFIG
environment_name = ""
bastion_inbound_ip =  []

# NETWORKING
vpc_instance_tenancy = "default"
vpc_enable_dns_support = true
vpc_enable_dns_hostnames = true
vpc_enable_classiclink = false
vpc_cidr_block = "10.0.0.0/16"
subnet_public_cidr_block  = ["10.0.0.0/24","10.0.2.0/24","10.0.4.0/24"]
subnet_private_cidr_block = ["10.0.1.0/24","10.0.3.0/24","10.0.5.0/24"]

# COMPUTING
bastion_instance_ami = "ami-xxxxxxxxxxx"
bastion_instance_type = "t2.micro"

jenkins_master_instance_ami = "ami-xxxxxxxxxxx"
jenkins_master_instance_type = "t2.micro"

webserver_instance_ami = "ami-xxxxxxxxxx"
webserver_instance_type = "t2.micro"

# PLACE YOUR KEY PAIRS HERE
bastion_key_name = "xxxx"
bastion_public_key = "ssh-rsa xxxxxxxx"

jenkins_master_key_name = "xxxx"
jenkins_master_public_key = "ssh-rsa xxxxxxxx"

webserver_key_name = "xxxx"
webserver_public_key = "ssh-rsa xxxxxxxx"

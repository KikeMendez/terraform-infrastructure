start_network:
	cd environments/$(ENV)/networking \
	&& terraform init \
	&& terraform apply -auto-approve -target=module.internet_network -var-file=../../.config/$(ENV).tfvars \
	&& terraform apply -auto-approve -target=module.private_subnet -var-file=../../.config/$(ENV).tfvars \
	&& terraform apply -auto-approve -target=module.public_route_table_association -var-file=../../.config/$(ENV).tfvars \
	&& terraform apply -auto-approve -target=module.private_route_table_association -var-file=../../.config/$(ENV).tfvars \

destroy_network:
	cd environments/$(ENV)/networking \
	&& terraform destroy -var-file=../../.config/$(ENV).tfvars --force

start_computers:
	make start_bastion ENV=$(ENV) \
	&& make start_webserver ENV=$(ENV) \
	&& make start_jenkins ENV=$(ENV) \

destroy_computers:
	make destroy_webserver ENV=$(ENV) \
	&& make destroy_jenkins ENV=$(ENV) \
	&& make destroy_bastion ENV=$(ENV) \


start_jenkins:
	cd environments/$(ENV)/computing/jenkins \
	&& terraform init \
	&& terraform apply -auto-approve -var-file=../../../.config/$(ENV).tfvars \

destroy_jenkins:
	cd environments/$(ENV)/computing/jenkins \
	&& terraform destroy -var-file=../../../.config/$(ENV).tfvars --force

start_bastion:
	cd environments/$(ENV)/computing/bastion \
	&& terraform init \
	&& terraform apply -auto-approve -var-file=../../../.config/$(ENV).tfvars \

destroy_bastion:
	cd environments/$(ENV)/computing/bastion \
	&& terraform destroy -var-file=../../../.config/$(ENV).tfvars --force

start_webserver:
	cd environments/$(ENV)/computing/webserver \
	&& terraform init \
	&& terraform apply -auto-approve -var-file=../../../.config/$(ENV).tfvars \

destroy_webserver:
	cd environments/$(ENV)/computing/webserver \
	&& terraform destroy -var-file=../../../.config/$(ENV).tfvars --force

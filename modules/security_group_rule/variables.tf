variable "type" {}
variable "from_port" {}
variable "to_port" {}
variable "protocol" {}
variable "security_group_id" {}
variable "cidr_block" {
  type = "list"
  default = []
}
variable "source_security_group_id" {
    default = ""
}
variable "sgr_cidr_block" {}

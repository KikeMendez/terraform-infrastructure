resource "aws_security_group_rule" "sgr_cidr_blocks" {
    count = "${var.sgr_cidr_block}"
    type            = "${var.type}"
    from_port       = "${var.from_port}"
    to_port         = "${var.to_port}"
    protocol        = "${var.protocol}"
    cidr_blocks     = "${var.cidr_block}" 
    security_group_id = "${var.security_group_id}"
}

resource "aws_security_group_rule" "sgr_group_id" {
    count = "${ 1 - var.sgr_cidr_block }"
    type            = "${var.type}"
    from_port       = "${var.from_port}"
    to_port         = "${var.to_port}"
    protocol        = "${var.protocol}"
    security_group_id = "${var.security_group_id}"
    source_security_group_id  = "${var.source_security_group_id}"
}

resource "aws_eip" "eip" {
    vpc      = true
    tags = {
      Name = "eip_${var.environment_name}"
    }
}

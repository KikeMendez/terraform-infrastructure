resource "aws_security_group" "bastion_sg" {
    count = "${var.bastion_security_group}"
    
    vpc_id      = "${var.vpc_id}"
    name        = "${var.sg_name}"
    description = "${var.description}"

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = "${var.ingress_cidr_blocks}"
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "sg-${var.sg_name}"
    }
}

resource "aws_security_group" "sg" {
    count = "${ 1 - var.bastion_security_group }"
    
    vpc_id      = "${var.vpc_id}"
    name        = "${var.sg_name}"
    description = "${var.description}"
    
    tags = {
        Name = "sg-${var.sg_name}"
    }
}

variable "vpc_id" {}
variable "sg_name" {}
variable "description" {}

variable "ingress_cidr_blocks" {
    type = "list"
    default = [""]
}

variable "bastion_security_group" {
    default = 0
}

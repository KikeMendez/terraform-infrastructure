
output "id" {
  value = "${aws_security_group.sg.*.id}"
}

output "name" {
  value = "${aws_security_group.sg.*.name}"
}

output "arn" {
  value = "${aws_security_group.sg.*.arn}"
}

output "vpc_id" {
  value = "${aws_security_group.sg.*.vpc_id}"
}


output "bastion_sg_id" {
  value = "${aws_security_group.bastion_sg.*.id}"
}

output "bastion_sg_name" {
  value = "${aws_security_group.bastion_sg.*.name}"
}

output "bastion_sg_arn" {
  value = "${aws_security_group.bastion_sg.*.arn}"
}

output "bastion_sg_vpc_id" {
  value = "${aws_security_group.bastion_sg.*.vpc_id}"
}


resource "aws_route53_record" "marshallOne" {
  name    = "${var.record_name}"
  type    = "A"
  zone_id = "${var.route53_zone_id}"

  alias {
    evaluate_target_health = true
    name                   = "${var.regional_domain_name}"
    zone_id                = "${var.regional_zone_id}"
  }
}
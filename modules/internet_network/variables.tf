variable "environment_name" {}
variable "vpc_id" {}
variable "vpc_default_route_table_id" {}
variable "public_subnets" { type = "list"}

variable "elastic_ip_id" {}

output "internet_gateway_id" {
  value = "${aws_internet_gateway.igw.id}"
}

output "aws_nat_gateway_id" {
   value = "${aws_nat_gateway.nat_gateway.id}"
}
output "nat_gateway_allocation_id" {
  value = "${aws_nat_gateway.nat_gateway.allocation_id}"
}

output "public_route_table_id" {
  value = "${aws_route_table.public_route_table.id}"
}

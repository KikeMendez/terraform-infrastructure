
resource "aws_nat_gateway" "nat_gateway" {

    allocation_id = "${var.elastic_ip_id}"
    subnet_id     = "${var.public_subnets[0]}"

    tags = {
        Name = "ngw-${var.environment_name}"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = "${var.vpc_id}"
    
    tags = {
        Name = "igw_${var.environment_name}"
    }
}

resource "aws_route_table" "public_route_table" {
    vpc_id = "${var.vpc_id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw.id}"
    }

    tags = {
        Name = "public_${var.environment_name}"
    }

}

resource "aws_default_route_table" "private_route_table" {
    default_route_table_id = "${var.vpc_default_route_table_id}"
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_nat_gateway.nat_gateway.id}"
    }
    
    tags = {
        Name = "private_${var.environment_name}"
    }
}

resource "aws_api_gateway_domain_name" "marshallOne" {
  domain_name              = "${var.domain_name}"
  regional_certificate_arn = "${var.certificate_arn}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

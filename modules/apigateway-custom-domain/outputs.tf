output "custom_domain_id" {
  value = "${aws_api_gateway_domain_name.marshallOne.id}"
}

output "regional_domain_name" {
  value = "${aws_api_gateway_domain_name.marshallOne.regional_domain_name}"
}

output "regional_zone_id" {
  value = "${aws_api_gateway_domain_name.marshallOne.regional_zone_id}"
}

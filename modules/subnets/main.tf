resource "aws_subnet" "subnet" {

  count = "${length(var.subnet_cidr_block)}"

  vpc_id  = "${var.vpc_id}"
  cidr_block = "${element(var.subnet_cidr_block, count.index)}"
  availability_zone = "${element(data.aws_availability_zones.azs.names, count.index)}"

  tags = {
    Name = "${var.subnet_name}"
  }
}

variable "environment_name" {}
# NETWORKING
variable "vpc_instance_tenancy" {}
variable "vpc_cidr_block" {}
variable "vpc_enable_dns_support" {}
variable "vpc_enable_dns_hostnames" {}
variable "vpc_enable_classiclink" {}

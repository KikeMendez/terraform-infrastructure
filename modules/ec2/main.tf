resource "aws_instance" "bastion" {
  count = "${var.is_bastion_server}"

  ami           = "${var.instance_ami}"
  key_name      = "${var.instance_key_name}"
  user_data = "${var.user_data}"

  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${var.vpc_security_group_ids}"]
  subnet_id = "${var.instance_subnet_id}"
  associate_public_ip_address = "${var.associate_public_ip_address}"

  tags = {
    Name = "${var.instance_tag_name}-${var.instance_name}"
  }
}

resource "aws_instance" "instance" {
  count = "${ 1 - var.is_bastion_server }"

  ami                    = "${var.instance_ami}"
  instance_type          = "${var.instance_type}"
  user_data = "${var.user_data}"
  key_name               = "${var.instance_key_name}"
  vpc_security_group_ids = ["${var.vpc_security_group_ids}"]
  subnet_id = "${var.instance_subnet_id}"
  associate_public_ip_address = "${var.associate_public_ip_address}"

  tags = {
    Name = "${var.instance_tag_name}-${var.instance_name}"
  }
}

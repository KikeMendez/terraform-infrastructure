output "bastion_id" {
  value = "${aws_instance.bastion.*.id}"
}
output "bastion_public_ip" {
  value = "${aws_instance.bastion.*.public_ip}"
}
output "bastion_private_ip" {
  value = "${aws_instance.bastion.*.private_ip}"
}

output "bastion_arn" {
   value = "${aws_instance.bastion.*.arn}"
}

output "id" {
  value = "${aws_instance.instance.*.id}"
}
output "public_ip" {
  value = "${aws_instance.instance.*.public_ip}"
}
output "private_ip" {
  value = "${aws_instance.instance.*.private_ip}"
}

output "arn" {
   value = "${aws_instance.instance.*.arn}"
}

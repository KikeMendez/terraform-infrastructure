variable "instance_ami" {}
variable "instance_name" {}
variable "instance_type" {}
variable "instance_key_name" {}

variable "instance_tag_name" {}

variable "associate_public_ip_address" {
  default = false
}

variable "is_bastion_server" {
    default = false
}

variable "vpc_security_group_ids" {
  type = "list"
}

variable "instance_subnet_id" {}

variable "user_data" {
  default = ""
}
